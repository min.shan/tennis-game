const ZERO_POINTS = "love";
const ONE_POINT = "fifteen";
const TWO_POINT = "thirty";
const NO_POINTS = "";
const SCORE_STRINGS = [ZERO_POINTS, ONE_POINT, TWO_POINT];
const game = {
  score(points) {
    return SCORE_STRINGS[points] || NO_POINTS;
  }
};

module.exports = game;
