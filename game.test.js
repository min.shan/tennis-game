const game = require("./game");
describe("Tennis game", () => {
  test('score should return "" if the points is ""', () => {
    expect(game.score("")).toBe("");
  });
  test('should return "love" when the score is 0', () => {
    expect(game.score(0)).toBe("love");
  });

  test('should return "fifteen" when the score is 1', () => {
    expect(game.score(1)).toBe("fifteen");
  });

  test('should return "thirty" when the score is 2', () => {
      expect(game.score(2)).toBe("thirty");
  });
});
